#Preface
This document contains information about the assignment environment solution, as well as information how to start the project.

#Environment
The environment is based on Docker configuration file generated on phpdocker.io and uses following containers:
* redis - _redis:alpine_
* mysql - _mysql:8.0_
* nginx - _nginx:alpine_
* php_fpm - based on _phpdockerio/php71-fpm:latest_

#First steps
In order to run the project, after cloning this repository:
 * Modify permissions for provided key - _chmod 400/public_rsa_
 * run the _bin/install_ command.

It will clone the source code from BitBucket and prepare Laravel.
If any error occurs while pulling the source code from BitBucket, please don't hesitate to contact me.

After the script will finish its work, you will have to store GitHub access token in database.
You can do it with using _artisan:tinker_ or by calling the API endpoint, as follows:
*curl 'http://localhost:1025/api/v1/github_tokens' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'Accept: application/json' --data 'token=38bc9d210b09f8d24dfa14d2fb206f6c6514c225'*

Navigate to http://localhost:1025 and everything should be working.